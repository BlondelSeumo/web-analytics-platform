<?php

return [

    'software' => [
        'name'      => 'phpAnalytics',
        'author'    => 'Lunatio',
        'url'       => 'https://lunatio.com/phpanalytics',
        'version'   => '2.2.0'
    ]

];
